# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 17-04-2023
### Fixed
- Main class pointing to wrong file name (`APIServer.kt` vs `ApiServer.kt`)


## [1.0.0] - 17-04-2023
### Added
- Initial release
- Gitlab CI configuration

## Changed
- Gradle CI image downgrade from yammy (default) to focal, due to Flapdoodle Mongo dependency