import java.text.SimpleDateFormat
import java.util.*

plugins {
  id("java")
  id("org.jetbrains.kotlin.jvm") version "1.8.20"
  id("com.google.cloud.tools.jib") version "3.3.1"
  kotlin("plugin.serialization") version "1.8.20"
}

group = "nl.scsn.broker.extension"
version = "1.0.0-SNAPSHOT"

repositories {
  mavenCentral()
}

java.targetCompatibility = JavaVersion.VERSION_17
java.sourceCompatibility = JavaVersion.VERSION_17
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
  kotlinOptions {
    jvmTarget = "17"
  }
}

val ktorVersion = "2.2.4"

dependencies {
  implementation("org.jetbrains.kotlin:kotlin-stdlib")
  implementation("io.ktor:ktor-server-core:$ktorVersion")
  implementation("io.ktor:ktor-server-netty:$ktorVersion")
  implementation("io.ktor:ktor-server-auth:$ktorVersion")
  implementation("io.ktor:ktor-server-status-pages:$ktorVersion")
  implementation("io.ktor:ktor-serialization-gson:$ktorVersion")
  implementation("io.ktor:ktor-server-content-negotiation:$ktorVersion")

  implementation("ch.qos.logback:logback-classic:1.4.6")
  implementation("com.google.code.gson:gson:2.10.1")
  implementation("com.google.guava:guava:31.1-jre")

  implementation("org.json:json:20230227")
  implementation("org.apache.httpcomponents.client5:httpclient5:5.2.1")

  implementation("org.bouncycastle:bcprov-jdk18on:1.72")

  implementation("org.litote.kmongo:kmongo-coroutine:4.8.0")

  implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.5.0")

  testImplementation ("org.junit.jupiter:junit-jupiter-api:5.9.2")
  testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.2")
  testImplementation("org.junit-pioneer:junit-pioneer:2.0.0")
  testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo:4.6.2")
  testImplementation("com.github.tomakehurst:wiremock-jre8:2.35.0")
}

tasks
  .withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompilationTask<*>>()
  .configureEach {
    compilerOptions
      .languageVersion
      .set(
        org.jetbrains.kotlin.gradle.dsl.KotlinVersion.KOTLIN_1_9
      )
  }

repositories {
  mavenCentral()
}

tasks.withType<Test> {
  useJUnitPlatform()
}

jib {
  from {
    image = "eclipse-temurin:17-jre"

    platforms {
      platform {
        architecture = "amd64"
        os = "linux"
      }
      platform {
        architecture = "arm64"
        os = "linux"
      }
    }

  }
  to {
    val imageName = System.getenv().getOrDefault("IMAGE_NAME", "docker.nexus.dataspac.es/scsn/broker-extension")
    val imageTag = System.getenv().getOrDefault("IMAGE_TAG", "local").replace('/', '-')
    val imageTags = mutableSetOf(imageTag)
    if (System.getenv().containsKey("CI")) {
      imageTags += "${imageTag}-${SimpleDateFormat("yyyyMMddHHmm").format(Date())}"
    }
    image = imageName
    tags = imageTags

  }

  container {
    user = "nobody"
    creationTime.set("USE_CURRENT_TIMESTAMP")
    ports = listOf("4001/tcp")
    mainClass = "nl.scsn.broker.ApiServerKt"
  }
}