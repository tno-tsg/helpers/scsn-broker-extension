package nl.scsn.broker

import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.Filters.or
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

data class AvailableGln(val available: Long)

fun Route.managementRoute() {
  get("/gln") {
    call.respond(GLNPersistence.getGln().toList().map { Party(it) })
  }
  get("/gln/available") {
    call.respond(AvailableGln(GLNPersistence.countAvailableGln()))
  }
  post("/gln") {
    val party = call.receive<Party>()
    val existing = GLNPersistence.getGln(or(eq("gln", party.PartyIdentification ?: "-1"), eq("brn", party.PartyLegalEntityID), eq("vat", party.PartyTaxID))).first()
    if (existing == null) {
      GLNPersistence.insertGln(
        PartyDocument(
          party.PartyIdentification ?: GLNPersistence.getAvailableGln(),
          party.PartyLegalEntityID,
          party.PartyTaxID,
          party.ServiceProviderID
        )
      )
      call.respond(HttpStatusCode.OK)
    } else {
      throw BadRequestException("PartyLegalEntityID or PartyTaxID already assigned to a PartyIdentification or provided PartyIdentification already exists (gln: ${existing.gln}")
    }
  }
  delete("/gln/{PartyIdentification}") {
    val partyIdentification = call.parameters["PartyIdentification"] ?: throw BadRequestException("Could not parse PartyIdentification")
    if (GLNPersistence.deleteGln(eq("gln", partyIdentification))) {
      call.respond(HttpStatusCode.OK)
    } else {
      call.respond(HttpStatusCode.NotFound)
    }
  }

}