package nl.scsn.broker

import com.google.gson.*
import io.ktor.http.*
import io.ktor.serialization.gson.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.bouncycastle.crypto.generators.BCrypt
import org.bouncycastle.crypto.generators.OpenBSDBCrypt
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.reflect.Type

class NotFoundException(message: String?): Exception(message)
class BadRequestException(message: String?): Exception(message)
class ForbiddenException(message: String?): Exception(message)
data class ErrorMessage(val status: String, val error: String, val reason: String?)

object Config {
  val sparqlUrl = System.getenv("SPARQL_URL") ?: throw Exception("No SPARQL_URL environment variable present")
  val mongoDbConnectionString = System.getenv("MONGODB_CONNECTIONSTRING") ?: throw Exception("No MONGODB_CONNECTIONSTRING environment variable present")
  val adminPassword = System.getenv("ADMIN_PASSWORD") ?: throw Exception("No ADMIN_PASSWORD environment variable present")
  val production = System.getenv("PRODUCTION")?.lowercase() == "true"
}

val LOG: Logger = LoggerFactory.getLogger("ApiServer")

fun main() {
  embeddedServer(Netty, 8080) {
    install(IgnoreTrailingSlash)
    install(ContentNegotiation) {
      gson {
        setPrettyPrinting()
        registerTypeAdapter(JSONObject::class.java, object : JsonSerializer<JSONObject>, JsonDeserializer<JSONObject> {
          override fun serialize(src: JSONObject?, typeOfSrc: Type?, context: JsonSerializationContext): JsonElement? {
            return src?.let {
              val jsonObject = JsonObject()
              src.keys().forEach { key ->
                val value = src.opt(key)
                val jsonElement = context.serialize(value, value.javaClass)
                jsonObject.add(key, jsonElement)
              }
              jsonObject
            }
          }

          override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
          ): JSONObject? {
            return json?.let {
              try {
                JSONObject(json.toString())
              } catch (e: JSONException) {
                throw JsonParseException(e)
              }
            }
          }
        })
        registerTypeAdapter(JSONArray::class.java, object : JsonSerializer<JSONArray>, JsonDeserializer<JSONArray> {
          override fun serialize(src: JSONArray?, typeOfSrc: Type?, context: JsonSerializationContext): JsonElement? {
            return src?.let {
              val jsonArray = JsonArray()
              src.forEach { value ->
                jsonArray.add(context.serialize(value, value.javaClass))
              }
              jsonArray
            }
          }

          override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
          ): JSONArray? {
            return json?.let {
              try {
                JSONArray(json.toString())
              } catch (e: JSONException) {
                throw JsonParseException(e)
              }
            }
          }
        })
      }
    }
    install(StatusPages) {
      exception<NotFoundException> { call, cause ->
        LOG.warn("NotFoundException: ${cause.message}")
        call.respond(HttpStatusCode.NotFound, ErrorMessage("Error", "Not Found", cause.message))
      }
      exception<BadRequestException> { call, cause ->
        LOG.warn("BadRequestException: ${cause.message}")
        call.respond(HttpStatusCode.BadRequest, ErrorMessage("Error", "Bad Request", cause.message))
      }
      exception<ForbiddenException> { call, cause ->
        LOG.warn("ForbiddenException: ${cause.message}")
        call.respond(HttpStatusCode.Forbidden, ErrorMessage("Error", "Forbidden", cause.message))
      }
      exception<Exception> { call, cause ->
        LOG.warn("${cause.javaClass.simpleName}: ${cause.message}")
        call.respond(HttpStatusCode.InternalServerError, ErrorMessage("Error", cause.javaClass.simpleName, cause.message))
      }
    }
    install(Authentication) {
      basic("management") {
        realm = "SCSN Broker GLN Management API"
        validate { credentials ->
          if (OpenBSDBCrypt.checkPassword(Config.adminPassword, credentials.password.encodeToByteArray())) {
            UserIdPrincipal(credentials.name)
          } else {
            null
          }
        }
      }
    }
    install(Routing) {
      route("/broker") {
        route("/party") {
          partyRoute()
        }
        route("/gln") {
          glnRoute()
        }
      }
      authenticate("management") {
        route("/api") {
          managementRoute()
        }
      }
    }
  }.start(wait = true)
}
