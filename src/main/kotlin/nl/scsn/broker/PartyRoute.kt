package nl.scsn.broker

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.json.JSONObject
import org.json.XML

fun Route.partyRoute() {
  get {
    val partyName = call.request.queryParameters["PartyName"] ?: call.request.queryParameters["name"]
    val partyLegalEntityID = call.request.queryParameters["PartyLegalEntityID"] ?: call.request.queryParameters["KVK"]
    val partyTaxId = call.request.queryParameters["PartyTaxID"] ?: call.request.queryParameters["TIN"]
    var filter = ""
    if (partyName?.isNotEmpty() == true) {
      filter += " FILTER regex(?title, \".*$partyName.*\", 'i') "
    }
    if (partyLegalEntityID?.isNotEmpty() == true) {
      filter += """
              ?scsnid ids:description ?partylegalid. 
              FILTER regex(?partylegalid, ".*$partyLegalEntityID.*", 'i')
              FILTER (lcase(lang(?partylegalid)) = "cac-c-partylegalentity-cbc-c-companyid")
            """.trimIndent()
    }
    if (partyTaxId?.isNotEmpty() == true) {
      filter += """
              ?scsnid ids:description ?partytaxid. 
              FILTER regex(?partytaxid, ".*$partyTaxId.*", 'i')
              FILTER (lcase(lang(?partytaxid)) = "cac-c-partytaxscheme-cbc-c-companyid")
            """
    }
    val sparql = baseSparql(filter)
    val result = BrokerProvider.scsnSelect(sparql)
    when (call.request.accept()) {
      "application/xml" -> {
        call.response.header(HttpHeaders.ContentType, "application/xml")
        call.respond("<?xml version=\"1.0\" encoding=\"UTF-8\"?>${XML.toString(JSONObject().put("Party", result), "Parties")}")
      }
      else -> {
        call.response.header(HttpHeaders.ContentType, "application/json")
        call.respond(result)
      }
    }
  }
  get("/{PartyIdentification}") {
    val partyIdentification = call.parameters["PartyIdentification"] ?: throw BadRequestException("Could not parse PartyIdentification")
    val sparql = if (partyIdentification.startsWith("urn")) {
      baseSparql("""FILTER (?scsnid = <$partyIdentification>)""")
    } else {
      baseSparql("""FILTER (?scsnid = <urn:scsn:$partyIdentification>)""")
    }
    val result = BrokerProvider.scsnSelect(sparql)
    when {
      result.length() == 0 -> throw NotFoundException("GLN $partyIdentification not found")
      call.request.accept() == "application/xml" -> {
        call.response.header(HttpHeaders.ContentType, "application/xml")
        call.respond("<?xml version=\"1.0\" encoding=\"UTF-8\"?>${XML.toString(result.getJSONObject(0), "Party")}")
      }
      else -> {
        call.response.header(HttpHeaders.ContentType, "application/json")
        call.respond(result.getJSONObject(0))
      }
    }
  }
}

private fun baseSparql(filter: String?): String {
  return """
      PREFIX ids: <https://w3id.org/idsa/core/>
      SELECT DISTINCT ?idsid ?connectortitle ?connectordescription ?title ?description ?apiSpec ?scsnid ?accessurl WHERE {
        GRAPH ?idsid {
          ?c a ids:TrustedConnector.
          ?c ids:title ?connectortitle.
          ?c ids:description ?connectordescription.
          ?c ids:resourceCatalog ?cat. 
          ?cat ids:offeredResource ?r. 
          ?r ids:sovereign ?scsnid.
          ?r ids:resourceEndpoint ?re.
          ?re ids:accessURL ?accessurl.
          ?re ids:endpointDocumentation ?apiSpec.
          ?r ids:title ?title.
          OPTIONAL { ?r ids:description ?description. }
          $filter
        }
      }
    """.trimIndent()
}