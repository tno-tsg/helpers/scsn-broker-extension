package nl.scsn.broker

import com.mongodb.client.model.Filters
import com.mongodb.client.model.Updates
import org.bson.Document
import org.bson.conversions.Bson
import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.coroutine.CoroutineFindPublisher
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo

data class PartyDocument(val gln: String, val brn: String, val vat: String, val spid: String)
data class GlnAvailabilityDocument(val gln: String, val available: Boolean)

object GLNPersistence {
  private var glnCollection: CoroutineCollection<PartyDocument>
  private var glnAvailabilityCollection: CoroutineCollection<GlnAvailabilityDocument>

  init {
    val mongoClient = KMongo.createClient(Config.mongoDbConnectionString).coroutine
    val db = mongoClient.getDatabase("broker")
    db.getCollection<PartyDocument>("gln")
    glnCollection = db.getCollection("gln")
    glnAvailabilityCollection = db.getCollection("glnprovisioning")
  }

  suspend fun countAvailableGln(): Long {
    return if (Config.production) {
      glnAvailabilityCollection.countDocuments(Filters.eq("available", true))
    } else {
      -1
    }
  }

  suspend fun getAvailableGln(): String {
    return if (Config.production) {
      val gln = glnAvailabilityCollection.findOneAndUpdate(
        Filters.eq("available", true),
        Updates.set("available", false)
      )
      gln?.gln ?: throw NotFoundException("Can't find available GLN")
    } else {
      System.currentTimeMillis().toString()
    }
  }

  fun getGln(filter: Array<Bson>? = null): CoroutineFindPublisher<PartyDocument> {
    val findFilter = if (filter != null && filter.isNotEmpty())
      Filters.and(*filter)
    else
      Document()

    return glnCollection.find(findFilter)
  }

  fun getGln(filter: Bson): CoroutineFindPublisher<PartyDocument> {
    return glnCollection.find(filter)
  }

  suspend fun insertGln(document: PartyDocument) {
    glnCollection.insertOne(document)
  }

  suspend fun deleteGln(filter: Bson): Boolean {
    val document = glnCollection.findOneAndDelete(filter)
    document?.let {
      glnAvailabilityCollection.findOneAndUpdate(Filters.eq("gln", document.gln), Updates.set("available", true))
    }
    return document != null
  }

}