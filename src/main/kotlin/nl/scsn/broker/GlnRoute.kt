package nl.scsn.broker

import com.mongodb.client.model.Filters
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.bson.conversions.Bson
import org.json.JSONObject

data class Party(val PartyIdentification: String?, val PartyLegalEntityID: String, val PartyTaxID: String, val ServiceProviderID: String) {
  constructor(partyDocument: PartyDocument): this(partyDocument.gln, partyDocument.brn, partyDocument.vat, partyDocument.spid)
}

fun Route.glnRoute() {
  get {
    val filter: MutableList<Bson> = mutableListOf()
    (call.request.queryParameters["PartyLegalEntityID"] ?: call.request.queryParameters["brn"] ?: call.request.queryParameters["kvk"])?.let {
      filter.add(Filters.eq("brn", it))
    }
    (call.request.queryParameters["PartyTaxID"] ?: call.request.queryParameters["vat"] ?: call.request.queryParameters["tin"])?.let {
      filter.add(Filters.eq("vat", it))
    }
    (call.request.queryParameters["PartyIdentification"] ?: call.request.queryParameters["gln"] ?: call.request.queryParameters["scsnid"])?.let {
      filter.add(Filters.eq("gln", it))
    }
    (call.request.queryParameters["ServiceProviderID"] ?: call.request.queryParameters["spid"])?.let {
      filter.add(Filters.eq("spid", it))
    }
    call.response.header(HttpHeaders.ContentType, "application/json")
    call.respond(
      GLNPersistence.getGln(filter.toTypedArray()).toList().map { Party(it) }
    )
  }
  post {
    val partyInfo = JSONObject(call.receiveText())
    val spid = call.request.header("TSG_ISSUER_CONNECTOR") ?: throw BadRequestException("Mandatory TSG_ISSUER_CONNECTOR header is not present")
    when {
      !partyInfo.has("PartyLegalEntityID") -> throw BadRequestException("Mandatory PartyLegalEntityID is missing in request")
      !partyInfo.has("PartyTaxID") -> throw BadRequestException("Mandatory PartyTaxID is missing in request")
    }
    val gln = if (partyInfo.has("PartyIdentification")) {
      partyInfo.getString("PartyIdentification")?.replace("urn:scsn:", "") ?: throw BadRequestException("PartyIdentification in request body cannot be parsed to a string")
    } else {
      GLNPersistence.getAvailableGln()
    }

    val brn = partyInfo.getString("PartyLegalEntityID")
    val vat = partyInfo.getString("PartyTaxID")
    val existing = GLNPersistence.getGln(
      Filters.or(
        Filters.eq("gln", gln),
        Filters.eq("brn", brn),
        Filters.eq("vat", vat)
      )).first()
    if (existing == null) {
      GLNPersistence.insertGln(PartyDocument(gln, brn, vat, spid))
      call.respond(
        HttpStatusCode.OK, JSONObject()
        .put("status", "Success")
        .put("binding", JSONObject()
          .put("PartyIdentification", gln)
          .put("PartyLegalEntityID", brn)
          .put("PartyTaxID", vat)
          .put("ServiceProviderID", spid)
        )
      )
    } else {
      throw BadRequestException("PartyLegalEntityID or PartyTaxID already assigned to a PartyIdentification or provided PartyIdentification already exists (existing GLN: ${existing.gln}")
    }
  }
  delete("/{PartyIdentification}") {
    val partyIdentification = call.parameters["PartyIdentification"] ?: throw BadRequestException("Could not parse PartyIdentification")
    val spid = call.request.header("TSG_ISSUER_CONNECTOR") ?: throw BadRequestException("Mandatory TSG_ISSUER_CONNECTOR header is not present")
    val document = GLNPersistence.getGln(Filters.eq("gln", partyIdentification)).first() ?: throw NotFoundException("GLN $partyIdentification not found")
    if (document.spid != spid) {
      throw ForbiddenException("The Service Provider field in the GLN registration does not correspond with the current requestor (${document.spid} vs $spid)")
    }
    if (GLNPersistence.deleteGln(Filters.eq("gln", partyIdentification))) {
      call.respond(HttpStatusCode.OK)
    } else {
      throw Exception("Unable to delete GLN allocation")
    }
  }
}