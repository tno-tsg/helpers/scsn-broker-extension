package nl.scsn.broker

import org.apache.hc.client5.http.classic.methods.HttpPost
import org.apache.hc.client5.http.impl.classic.HttpClients
import org.apache.hc.core5.http.ContentType
import org.apache.hc.core5.http.io.entity.StringEntity
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.util.regex.Pattern

object BrokerProvider {
  private val ublList = listOf("cac:Address", "cac:AddressLine", "cac:Contact", "cac:Country", "cac:LocationCoordinate", "cac:OtherCommunication", "cac:PartyIdentification", "cac:PartyLegalEntity", "cac:PartyName", "cac:PartyTaxScheme", "cac:Person", "cac:PhysicalLocation", "cac:PostalAddress", "cbc:BuildingNumber", "cbc:CityName", "cbc:CompanyID", "cbc:CoordinateSystemCode", "cbc:Description", "cbc:ElectronicMail", "cbc:FamilyName", "cbc:FirstName", "cbc:ID", "cbc:IdentificationCode", "cbc:JobTitle", "cbc:LatitudeDegreesMeasure", "cbc:Line", "cbc:LongitudeDegreesMeasure", "cbc:Name", "cbc:OrganizationDepartment", "cbc:PostalZone", "cbc:StreetName", "cbc:Telefax", "cbc:Telephone", "cbc:Title", "cbc:Value")

  private fun sparqlSelect(query: String?): JSONArray {
    val httpPost = HttpPost(Config.sparqlUrl)
    httpPost.setHeader("Content-Type", "application/sparql-query")
    httpPost.setHeader("Accept", "application/sparql-results+json")
    httpPost.entity = StringEntity(query, ContentType.create("application/sparql-query"))
    return try {
      HttpClients.createDefault().execute(httpPost) { response ->
        val resultJson = String(response.entity.content.readAllBytes())
        JSONObject(resultJson).getJSONObject("results").getJSONArray("bindings")
      }
    } catch (e: IOException) {
      JSONArray()
    }
  }

  fun scsnSelect(query: String): JSONArray {
    val jsonArray = sparqlSelect(query)
    val map = mutableMapOf<String, MutableMap<String, MutableMap<String, JSONObject>>>()
    val languages = arrayOf("en", "nl")
    jsonArray.filterIsInstance<JSONObject>()
      .filter {
        (
            !it.getJSONObject("connectortitle").has("xml:lang")
                || it.getJSONObject("connectortitle").getString("xml:lang") in languages
            ) &&
            (
                !it.getJSONObject("connectordescription").has("xml:lang")
                    || it.getJSONObject("connectordescription").getString("xml:lang") in languages
                )
      }.forEach {
        val scsnId = it.getJSONObject("scsnid").getString("value")
        val idsId = it.getJSONObject("idsid").getString("value")
        val version = it.getJSONObject("apiSpec").getString("value").split("/").last()
        if (!map.containsKey(scsnId)) {
          map[scsnId] = mutableMapOf()
        }
        if (!map[scsnId]!!.containsKey(idsId)) {
          map[scsnId]!![idsId] = mutableMapOf()
        }
        if (!map[scsnId]!![idsId]!!.containsKey(version)) {
          map[scsnId]!![idsId]!![version] = JSONObject()
        }
        map[scsnId]!![idsId]!![version]!!
          .put("cac:PartyIdentification.cbc:ID", scsnId)
          .put("cac:PartyName.cbc:Name", it.getJSONObject("title").opt("value"))
          .put("connector.idsId", idsId)
          .put("connector.accessUrl", it.getJSONObject("accessurl").opt("value"))
          .put("connector.title", it.getJSONObject("connectortitle").opt("value"))
          .put("connector.version", version).apply {
            if (it.has("description")) {
              put(replaceUBL(it.getJSONObject("description").optString("xml:lang")), it.getJSONObject("description").optString("value"))
            }
          }
      }

    val t = map.values.map { e ->
      e.values.map { e2 ->
        e2.map { e3 ->
          val tmp = JSONObject()
          e3.value.toMap().forEach {
            val tree = explode(it.key, it.value)
            merge(tmp, tree)
          }
          tmp
        }
      }

    }

    return JSONArray(t.map {
      val list = it.flatten()
      list[0].put("connector", JSONArray(
        list.map { it.getJSONObject("connector") }
      ))
      return@map list[0]
    })
  }

  private fun replaceUBL(key: String): String {
    var parsedKey = key.replace(Pattern.compile("-c-", Pattern.CASE_INSENSITIVE).toRegex(), ":").replace('-', '.')
    ublList.forEach { parsedKey = parsedKey.replace(Pattern.compile(it, Pattern.CASE_INSENSITIVE).toRegex(), it) }
    return parsedKey
  }

  private fun explode(key: String, value: Any): JSONObject {
    val split = key.split(".", limit = 2)
    return if (split.size > 1) {
      JSONObject().put(split[0], explode(split[1], value))
    } else {
      JSONObject().put(key, value)
    }
  }

  private fun merge(a: JSONObject, b: JSONObject) {
    val key = b.keys().next()
    if (b[key] is JSONObject) {
      if (a.has(key)) {
        merge(a[key] as JSONObject, b[key] as JSONObject)
        a.put(key, a[key])
      } else {
        a.put(key, b[key])
      }
    } else {
      a.put(key, b[key])
    }
  }
}