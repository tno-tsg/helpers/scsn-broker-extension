package nl.scsn.broker

import com.mongodb.client.model.Filters
import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.mongo.transitions.Mongod
import de.flapdoodle.embed.mongo.transitions.RunningMongodProcess
import de.flapdoodle.reverse.TransitionWalker
import de.flapdoodle.reverse.transitions.Start
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.junitpioneer.jupiter.SetEnvironmentVariable
import org.junitpioneer.jupiter.SetEnvironmentVariable.SetEnvironmentVariables

@ExtendWith(MongoTestExtension::class)
@SetEnvironmentVariable(key = "MONGODB_CONNECTIONSTRING", value = "mongodb://localhost:52632")
@SetEnvironmentVariable(key = "SPARQL_URL", value = "http://localhost:13030/connectorData")
@SetEnvironmentVariable(key = "ADMIN_PASSWORD", value = "$2a$12\$wRVa/RX10BvUya1e5KMnBuXS2Mo9uCaFcdi9vHIWfKIHSgTNmdXze")
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class GLNPersistenceTest {
  @Order(0)
  @Test
  fun insertGln() {
    runBlocking {
      GLNPersistence.insertGln(
        PartyDocument(
          gln = "1234567",
          brn = "BRN1234567",
          vat = "VAT1234567",
          spid = "urn:tsg:ServiceProvider"
        )
      )
      GLNPersistence.insertGln(
        PartyDocument(
          gln = "2345678",
          brn = "BRN2345678",
          vat = "VAT2345678",
          spid = "urn:tsg:ServiceProvider"
        )
      )

      Assertions.assertEquals(2, GLNPersistence.getGln(emptyArray()).toList().size)
    }
  }

  @Order(1)
  @Test
  fun deleteGln() {
    runBlocking {
      GLNPersistence.deleteGln(Filters.eq("gln", "1234567"))
      Assertions.assertEquals(1, GLNPersistence.getGln(emptyArray()).toList().size)
    }
  }
}