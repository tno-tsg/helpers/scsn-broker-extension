package nl.scsn.broker

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junitpioneer.jupiter.SetEnvironmentVariable

@SetEnvironmentVariable(key = "MONGODB_CONNECTIONSTRING", value = "mongodb://localhost:52632")
@SetEnvironmentVariable(key = "SPARQL_URL", value = "http://localhost:48714/connectorData")
@SetEnvironmentVariable(key = "ADMIN_PASSWORD", value = "$2a$12\$wRVa/RX10BvUya1e5KMnBuXS2Mo9uCaFcdi9vHIWfKIHSgTNmdXze")
class BrokerProviderTest {

  @Test
  fun testSCNSSelect() {
    val test = BrokerProvider.scsnSelect("")
    Assertions.assertEquals(1, test.length())
    Assertions.assertEquals("urn:playground:tsg:connectors:TestConsumer:AgentA", test.getJSONObject(0).getJSONObject("cac:PartyIdentification").getString("cbc:ID"))
    Assertions.assertEquals("0.9.2", test.getJSONObject(0).getJSONArray("connector").getJSONObject(0).getString("version"))
  }

  companion object {
    /** WireMock server */
    private lateinit var wireMock: WireMockServer

    /** Setup WireMock server with Broker response mocks */
    @JvmStatic
    @BeforeAll
    fun setupWireMock() {
      wireMock = WireMockServer(48714).apply { start() }

      wireMock.stubFor(
        WireMock.post("/connectorData")
          .willReturn(
            WireMock.aResponse()
              .withStatus(200)
              .withBody("""
                {
                  "head": {
                    "vars": [ "idsid" , "connectortitle" , "connectordescription" , "title" , "description" , "apiSpec" , "scsnid" , "accessurl" ]
                  } ,
                  "results": {
                    "bindings": [
                      {
                        "idsid": { "type": "uri" , "value": "urn:playground:tsg:connectors:TestConnector" } ,
                        "connectortitle": { "type": "literal" , "value": "Playground Test Connector Container" } ,
                        "connectordescription": { "type": "literal" , "value": "Playground Test Connector Container" } ,
                        "title": { "type": "literal" , "xml:lang": "en" , "value": "Test Consumer Agent A" } ,
                        "apiSpec": { "type": "uri" , "value": "https://app.swaggerhub.com/apiproxy/registry/I814/httpbin/0.9.2" } ,
                        "scsnid": { "type": "uri" , "value": "urn:playground:tsg:connectors:TestConsumer:AgentA" } ,
                        "accessurl": { "type": "uri" , "value": "https://test-connector.playground.dataspac.es/router" }
                      }
                    ]
                  }
                }
              """.trimIndent()
              )
          )
      )
    }
  }
}