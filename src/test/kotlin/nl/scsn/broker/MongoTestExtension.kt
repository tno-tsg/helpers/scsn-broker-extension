package nl.scsn.broker

import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.mongo.transitions.Mongod
import de.flapdoodle.embed.mongo.transitions.RunningMongodProcess
import de.flapdoodle.reverse.TransitionWalker
import de.flapdoodle.reverse.transitions.Start
import org.junit.jupiter.api.extension.*

class MongoTestExtension : BeforeAllCallback, AfterAllCallback {
  private lateinit var mongod: TransitionWalker.ReachedState<RunningMongodProcess>

  override fun beforeAll(context: ExtensionContext?) {
    mongod = Mongod.builder()
      .net(Start.to(Net::class.java)
        .initializedWith(
          Net.defaults().withPort(52632)))
      .build().start(Version.Main.V5_0)
  }

  override fun afterAll(context: ExtensionContext?) {
    mongod.current().stop()
  }
}