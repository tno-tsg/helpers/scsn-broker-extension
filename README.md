# SCSN Broker Extension

This repository contains the implementation of the API as specified in the [Broker 1.0.0 OpenAPI specification](https://api-specs.ids.smart-connected.nl/broker/1.0.0).

The Smart Connected Supplier Network Broker provides an abstraction layer on top of the International Data Spaces (IDS) Broker, to facilitate easy access to party information within the network. The Address Book functionality provides information of parties in a format similar to the Party information in the SCSN standard, with additional information on the way this party can be reached via the IDS infrastructure.

Next to the Address Book functionality, this Broker specification also supports the provisioning of unique identifiers based on the Global Location Number (GLN) standard. Parties, or Service Providers managing parties, can provision existing GLNs to be incorporated into the Broker, but also the provisioning of new GLNs is supported.

## Deployment

This service is intended to be deployed next to a [TSG Core Container](https://gitlab.com/tno-tsg/core-container) and [TSG OpenAPI Data App](https://gitlab.com/tno-tsg/data-apps/openapi).

By using the [Connector Helm Chart](https://gitlab.com/tno-tsg/helm-charts/connector), this service can be easily deployed with the right configuration.

The required block in the `values.yaml` file will look like:
```yaml
containers:
# ...
- type: helper
  image: docker.nexus.dataspac.es/scsn/broker-extension:1.0.0
  name: broker-extension
  environment:
    - name: ADMIN_PASSWORD
      value: $2a$12$wRVa/RX10BvUya1e5KMnBuXS2Mo9uCaFcdi9vHIWfKIHSgTNmdXze
    - name: MONGODB_CONNECTIONSTRING
      value: mongodb://root:password@mongo:27017/?authSource=admin
    - name: SPARQL_URL
      value: http://fuseki:3030/connectorData
    - name: PRODUCTION
      value: "false"
  services:
    - port: 8080
      name: http
# ...
```

Where:
- `ADMIN_PASSWORD` is a BCrypt encoded password for the admin user
- `MONGODB_CONNECTIONSTRING` is the MongoDB connection string to connect to a MongoDB instance or replicaset
- `SPARQL_URL` is the SPARQL endpoint of the triple store of the Broker (e.g. Fuseki)
- `PRODUCTION` is an optional boolean (defaults to `false`) that indicates whether the GLN provisioning should use the `glnprovisioning` collection in the MongoDB to hand out GLNs. If set to false, or omitted, dummy GLNs based on the current timestamp are handed out.

## Structure

This service is built around a Ktor HTTP server (in `ApiServer.kt`), with routes for:
- Parties (`/broker/party`):  in `PartyRoute.kt`  
  The parties are retrieved using the `BrokerProvider` which executes a SPARQL-query onto `SPARQL_URL` and formats the results into a workable format for the API.
- GLNs (`/broker/gln`):  in `GlnRoute.kt`  
  The GLNs are retrieved using the `GLNPersistence` which connects to a MongoDB. The document structure inside MongoDB follows `PartyDocument` and `GlnAvailabilityDocument`
- Management (`/api`):  in `ManagementRoute.kt` (requires BASIC authentication with the password defined in `ADMIN_PASSWORD`)  
  The management API is not contained in the Broker OpenAPI specification and should only be used for UI interactions.

> _Note_: Make sure to not expose `/broker` to an interface but only `/api`, since there is no authentication on the `/broker` endpoints as these are handled via the IDS connector 

## Build

The broker extension is built via Gradle, via the Gradle wrapper, usually directly into a Docker image by using Jib.

To locally build the project (and execute tests) execute:
```bash
./gradlew build
```
Or to only build and not run tests:
```bash
./gradlew assemble
```

To just execute the tests, execute:
```bash
./gradlew test
```

> _Note_: The tests use the Flapdoodle MongoDB dependency that downloads the MongoDB server for your machine as well as Wiremock to provide a mock service for HTTP requests. Both will run on a specific port that must be available.

To build the Docker image via the Google Jib extension, execute:
```bash
./gradlew jib
```
This automatically builds the Docker image `docker.nexus.dataspac.es/scsn/broker-extension:local` and pushes it to the Docker registry.
By setting the `IMAGE_NAME` and/or `IMAGE_TAG` environment variables the Docker image can be modified.

To prevent pushing the Docker image being pushed to the registry, execute either:
```bash
./gradlew jibDockerBuild
```
To build the image via a Docker daemon (requires a Docker daemon to be available), or:
```bash
./gradlew jibBuildTar
```
To build the image into a tarball.

